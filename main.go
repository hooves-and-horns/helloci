package main

import (
	"log"
	"strconv"
	"time"
)

func main() {
	log.Println("Hello CI!", time.Now().UnixMilli())

	val, _ := strconv.Atoi("qwe")
	log.Println(val, "long line long line")
}
